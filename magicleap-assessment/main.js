(function(){
	window.onload = function() {
		$(document).ready(function(){
			/*Application Container*/

			var mockData;

			/*Utility Functions*/
			function changeInnerByDataValue(dataValue,newInner) {
				$("*[data-value=" + dataValue + "]").html(newInner);
			}
			function getParameterByName(name, url) {
				if (!url) url = window.location.href;
				name = name.replace(/[\[\]]/g, "\\$&");
				var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
				if (!results) return null;
				if (!results[2]) return '';
				return decodeURIComponent(results[2].replace(/\+/g, " "));
			}
			function findSpaceshipByName(name) {
				var tempValue = -1;
				mockData.products.forEach(function(value){
					if(value.name == name) {
						tempValue = value;
					}
				});
				return tempValue;
			}

			/*Application Entry Point*/
			function applicationLoaded() {
				homePage();	
				productPage();	
			}

			function homePage() {
				// Functionality for home page.
				if($("script#spaceship-spec-thumbnail").length!=0) {

					console.log("Home Page Hit", new Date());

					// Declare variables.
					var spaceshipHtml = [];
					var containerSelectorToPlaceIn = "div#spaceship-spec-thumbnail-container";
					var templateHtml = $("script#spaceship-spec-thumbnail").html();

					// Go through each of the spaceships and create an element, based on the template.
					mockData.products.forEach(function(value,index){
						console.log(value);
						var randomIntBetween1and4 = (Math.floor(Math.random() * 4) + 1);
						var newSpaceship = templateHtml.replace("{{imageSource}}","spaceship"+randomIntBetween1and4+".png");
						newSpaceship = newSpaceship.replace("{{imageAltText}}",value.name+" spaceship");
						newSpaceship = newSpaceship.replace("{{captionTitle}}",value.name);
						newSpaceship = newSpaceship.replace("{{captionContent}}",value.manufacturer);
						newSpaceship = newSpaceship.replace("{{linkout}}","productspec.html?name="+value.name)
						spaceshipHtml.push(newSpaceship);
					});

					// Append all of the spaceships to the container.
					$(containerSelectorToPlaceIn).append(spaceshipHtml.join(""));
				}	
			}

			function productPage() {
				if($("script#spaceship-spec").length!=0) {
					console.log("Product Page Hit", new Date());
					if(getParameterByName("name").length!=0) {
						var spaceship = findSpaceshipByName(getParameterByName("name"));
						changeInnerByDataValue("spaceship-name",spaceship.name);
						changeInnerByDataValue("spaceship-class",spaceship.class);
						changeInnerByDataValue("spaceship-price",spaceship.price);
						changeInnerByDataValue("spaceship-length",spaceship.techspecs.length);
						changeInnerByDataValue("spaceship-maxaccel",spaceship.techspecs.maxaccel);
						changeInnerByDataValue("spaceship-mglt",spaceship.techspecs.mglt);
						changeInnerByDataValue("spaceship-maxatmospericspeed",spaceship.techspecs.maxatmospericspeed);
						changeInnerByDataValue("spaceship-shielding",spaceship.techspecs.shielding);
						changeInnerByDataValue("spaceship-hull",spaceship.techspecs.hull);
						changeInnerByDataValue("spaceship-sensor",spaceship.techspecs.sensor);
						changeInnerByDataValue("spaceship-targeting",spaceship.techspecs.targeting);
						changeInnerByDataValue("spaceship-arnament",spaceship.techspecs.arnament);
						changeInnerByDataValue("spaceship-communications",spaceship.techspecs.communications);
						if(Typed!=undefined) {
							Typed.new('.element', {
						      strings: [spaceship.name],
						      typeSpeed: 0
						    });
						}
						var randomIntBetween1and4 = (Math.floor(Math.random() * 4) + 1);
						$("#spaceshipImage").attr("src","spaceship"+randomIntBetween1and4+".png");
					}
				}

		}

			// Retrieve the spaceship infomation.
			$.getJSON("http://demo7475333.mockable.io/spaceships",function(data){
				// Set the mock data.
				mockData = data;
				applicationLoaded();

				console.log("Application Loaded", new Date());
			});
		});
	};
})();